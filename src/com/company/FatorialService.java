package com.company;

import java.util.Random;

public class FatorialService {



    public Integer obterValor(){

        Random rd = new Random();

        Integer valor = rd.nextInt(101);

        return valor;
    }

    public Double fatorar(Integer valor){

        Double fatorial;

        fatorial = valor.doubleValue();

        if(valor == 0 || valor == 1){
            return 1D;
        }
        do {

            valor --;
            fatorial *= valor;

        }while (valor > 1);

        return fatorial;

    }

    public void imprimirFatorial(Double fatorial, Integer valor){

        System.out.print("O fatorial de " + valor + " é " + fatorial.intValue());
    }
}
