package test;

import com.company.FatorialService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class TesteFatorial {

    private FatorialService fatService;
    private ByteArrayOutputStream out;

    @Before
    public void setup(){
        this.fatService = new FatorialService();
        this.out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(this.out));

    }

    //Testes metódo de obter Fatoriaal
    @Test
    public void testarFatorialO(){

        Double result = this.fatService.fatorar(0);

        Double expected = 1D;

        Assert.assertEquals(expected, result);
    }

    @Test
    public void testarFatorial1(){
        Double result = this.fatService.fatorar(1);

        Double expected = 1D;

        Assert.assertEquals(expected, result);
    }

    @Test
    public void testarFatorial2(){
        Double result = this.fatService.fatorar(2);

        Double expected = 2D;

        Assert.assertEquals(expected, result);
    }

    @Test
    public void testarFatorial3(){
        Double result = this.fatService.fatorar(3);

        Double expected = 6D;

        Assert.assertEquals(expected, result);
    }


    @Test
    public void testarFatorial4(){
        Double result = this.fatService.fatorar(4);

        Double expected = 24D;

        Assert.assertEquals(expected, result);
    }

    @Test
    public void testarFatorial5(){
        Double result = this.fatService.fatorar(5);

        Double expected = 120D;

        Assert.assertEquals(expected, result);
    }

    @Test
    public void testarFatorial6(){
        Double result = this.fatService.fatorar(6);

        Double expected = 720D;

        Assert.assertEquals(expected, result);
    }

    @Test
    public void testarFatorial7(){
        Double result = this.fatService.fatorar(7);

        Double expected = 5040D;

        Assert.assertEquals(expected, result);
    }

    @Test
    public void testarFatorial8(){
        Double result = this.fatService.fatorar(8);

        Double expected = 40320D;

        Assert.assertEquals(expected, result);
    }

    @Test
    public void testarFatorial9(){
        Double result = this.fatService.fatorar(9);

        Double expected = 362880D;

        Assert.assertEquals(expected, result);
    }

//Testes metódo de obter valor
    @Test
    public void testarObterValorMaiorIgual0(){

        Integer result = this.fatService.obterValor();

        Assert.assertTrue(result >= 0);
    }

    @Test
    public void testarObterValorMenorIgual100(){

        Integer result = this.fatService.obterValor();

        Assert.assertTrue(result <= 100);
    }

    //Testes metódo de impressão
    @Test
    public void testarImprimirFatorial0(){
        fatService.imprimirFatorial(1D, 0);
        String result = out.toString();
        String expected = "O fatorial de 0 é 1";

        Assert.assertEquals(expected, result);
    }

    @Test
    public void testarImprimirFatorial1(){
        fatService.imprimirFatorial(1D, 1);
        String result = out.toString();
        String expected = "O fatorial de 1 é 1";

        Assert.assertEquals(expected, result);
    }

    @Test
    public void testarImprimirFatorial5(){
        fatService.imprimirFatorial(120D, 5);
        String result = out.toString();
        String expected = "O fatorial de 5 é 120";

        Assert.assertEquals(expected, result);
    }
}
